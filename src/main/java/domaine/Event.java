package domaine;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

public class Event {
    private LocalDateTime dateDebut;
    private List<Artist> artistes;
    private String description;

    public Event(LocalDateTime dateDebut, List<Artist> artistes, String description) {
        this.dateDebut = Objects.requireNonNull(dateDebut);
        this.artistes = Objects.requireNonNull(artistes);
        this.description = Objects.requireNonNull(description);
    }
}
