package domaine;
import java.time.LocalDate;
import java.util.Objects;

public class Artist {
    private String name;
    private int careerStart;
    private String description;

    public Artist(String name, int careerStart, String description) {
        this.name = Objects.requireNonNull(name);
        this.careerStart = careerStart;
        this.description = Objects.requireNonNull(description);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = Objects.requireNonNull(name);
    }

    public int getCareerStart() {
        return careerStart;
    }

    public void setCareerStart(int careerStart) {
        this.careerStart = careerStart;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = Objects.requireNonNull(description);
    }
}

