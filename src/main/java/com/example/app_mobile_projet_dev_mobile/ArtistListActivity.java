package com.example.app_mobile_projet_dev_mobile;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;

public class ArtistListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artist_list);
        //RequestQueue requestQueue  = Volley.newRequestQueue(this);
        final TextView textView = (TextView) findViewById(R.id.test);
// ...

// Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this);
        String url ="http://192.168.1.88:8080/Projet_DEV_Mobile_war_exploded/artistes";

// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        textView.setText("Response is: "+ response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                textView.setText(error.getMessage());
            }
        });

// Add the request to the RequestQueue.
        queue.add(stringRequest);

    }
/*
    private String retrieveArtistsJSON(RequestQueue requestQueue) {
        String url = "http://192.168.1.88:8080/Projet_DEV_Mobile_war_exploded/artistes";
        JsonArrayRequest artistsRetrievalRequest = new JsonArrayRequest(url, this.recupererResponseListenerGetArtistes(), this.recupererErrorListenerGetArtistes() );
        requestQueue.add(artistsRetrievalRequest);
        return "toto";
    }
    private Response.Listener<JSONArray> recupererResponseListenerGetArtistes() {
        Response.Listener<JSONArray> responseListener = new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                TextView textView = (TextView) findViewById(R.id.test);
                textView.setText(response.toString());
            }
        };
        return responseListener;
    }

    private Response.ErrorListener recupererErrorListenerGetArtistes() {
        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        };
        return errorListener;
    }
*/

}