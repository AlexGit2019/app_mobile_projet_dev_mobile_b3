package com.example.app_mobile_projet_dev_mobile;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button consulterListeArtistes = (Button) findViewById(R.id.consulter_liste_artistes);
        consulterListeArtistes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startArtistListActivity = new Intent(MainActivity.this, ArtistListActivity.class);
                startActivity(startArtistListActivity);
            }
        });
    }

}